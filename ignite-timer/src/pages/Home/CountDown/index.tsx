import { useContext, useEffect } from 'react';
import { CountDownContainer, Separator } from './styles';
import { differenceInSeconds } from 'date-fns';
import { CycleContext } from '../../../contexts/CyclesContext';

export const CountDown = () => {
  const {
    activeCycle,
    activeCycleId,
    setCycleToFinished,
    secondsOff,
    setSecondsPassed,
  } = useContext(CycleContext);

  const totalSeconds = activeCycle ? activeCycle.minutes * 60 : 0;

  useEffect(() => {
    let interval: number;

    if (activeCycle) {
      interval = setInterval(() => {
        const secDifference = differenceInSeconds(
          new Date(),
          new Date(activeCycle.startDate),
        );

        if (secDifference >= totalSeconds) {
          setCycleToFinished();
          setSecondsPassed(totalSeconds);
          clearInterval(interval);
        } else {
          setSecondsPassed(secDifference);
        }
      }, 1000);
    }

    return () => {
      clearInterval(interval);
    };
  }, [
    activeCycle,
    totalSeconds,
    activeCycleId,
    setCycleToFinished,
    setSecondsPassed,
  ]);

  const currentSeconds = activeCycle ? totalSeconds - secondsOff : 0;
  const minutesLeft = Math.floor(currentSeconds / 60);
  const secondsLeft = currentSeconds % 60;

  const handleMinutes = String(minutesLeft).padStart(2, '0');
  const handleSeconds = String(secondsLeft).padStart(2, '0');

  useEffect(() => {
    activeCycle && (document.title = `${handleMinutes}:${handleSeconds}`);
  }, [activeCycle, handleMinutes, handleSeconds]);

  return (
    <CountDownContainer>
      <span>{handleMinutes[0]}</span>
      <span>{handleMinutes[1]}</span>

      <Separator>:</Separator>

      <span>{handleSeconds[0]}</span>
      <span>{handleSeconds[1]} </span>
    </CountDownContainer>
  );
};
