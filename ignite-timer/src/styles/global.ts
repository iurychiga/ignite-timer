import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    *{
        margin: 0;
        padding: 0;
        outline: none;
        box-sizing: border-box;
        text-decoration: none;
        list-style: none;
    }

    body{
        background: #333;
        color:#fff;
    }

    button {
        cursor: pointer;
    }

    body, input, textarea, button {
        font: 1rem 'Roboto', sans-serif;
    } 

`;
