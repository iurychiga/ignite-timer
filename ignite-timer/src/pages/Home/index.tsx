import { HandPalm, Play } from 'phosphor-react';
import { HomeContainer, StartCountDown, StopCountDown } from './styles';
import { NewCyclesFormData, newCycleFormValidateSchema } from '../../interfaces/validator';
import { NewCycleForm } from './NewCycleForm';
import { CountDown } from './CountDown';
import { FormProvider, useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { useContext } from 'react';
import { CycleContext } from '../../contexts/CyclesContext';

export const Home = () => {
  const { activeCycle, createNewCycle, InterruptCurrentCycle } =
    useContext(CycleContext);

  const newCycleForm = useForm<NewCyclesFormData>({
    resolver: zodResolver(newCycleFormValidateSchema),
    defaultValues: {
      minutes: 0,
      task: '',
    },
  });

  const { handleSubmit, watch, reset } = newCycleForm;

  const isSubmitDisabled = !watch('task');

  const handleCreateNewCycle = (data: NewCyclesFormData) => {
    createNewCycle(data);
    reset();
  };

  return (
    <HomeContainer>
      <form onSubmit={handleSubmit(handleCreateNewCycle)}>
        <FormProvider {...newCycleForm}>
          <NewCycleForm />
        </FormProvider>
        <CountDown />

        {activeCycle ? (
          <StopCountDown type='button' onClick={InterruptCurrentCycle}>
            <HandPalm size={24} />
            Interromper
          </StopCountDown>
        ) : (
          <StartCountDown type='submit' disabled={isSubmitDisabled}>
            Começar
            <Play size={24} />
          </StartCountDown>
        )}
      </form>
    </HomeContainer>
  );
};
