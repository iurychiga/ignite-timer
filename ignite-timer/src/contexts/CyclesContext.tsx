import { createContext, useState, useReducer, useEffect } from 'react';
import {
  CreateCycleData,
  iContextProps,
  iCycle,
  iCycleContext,
} from '../interfaces/validator';
import { cyclesReducer } from '../reducers/cycles/reducer';
import { InterruptCurrentCycleAction, addNewCycleAction, setCycleToFinishedAction } from '../reducers/cycles/actions';
import { differenceInSeconds } from 'date-fns';

export const CycleContext = createContext({} as iCycleContext);

export const CyclesContextProvider = ({ children }: iContextProps) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/no-unused-vars
  const [cyclesState, dispatch] = useReducer( cyclesReducer ,
    {
      cycles: [],
      activeCycleId: null,
    }, (initialState) => {
      const storedStateAsJSON = localStorage.getItem('@timer:cycles-1.0.0')

      if(storedStateAsJSON) {
        return JSON.parse(storedStateAsJSON)
      }

      return initialState
    }
  );

  const { cycles, activeCycleId } = cyclesState;

  const activeCycle = cycles.find((cycle) => cycle.id === activeCycleId);

  const [secondsOff, setSecondsOff] = useState(() => {
    if(activeCycle) {
      return differenceInSeconds(new Date(), new Date(activeCycle.startDate),
      );
    }

    return 0
  });

  const setSecondsPassed = (seconds: number) => {
    setSecondsOff(seconds);
  };

  const setCycleToFinished = () => {
    dispatch(setCycleToFinishedAction());
  };

  const createNewCycle = (data: CreateCycleData) => {
    const id = String(new Date().getTime());

    const newCycle: iCycle = {
      id,
      task: data.task,
      minutes: data.minutes,
      startDate: new Date(),
    };

    dispatch(addNewCycleAction(newCycle));
    setSecondsOff(0);
  };

  const InterruptCurrentCycle = () => {
    dispatch(InterruptCurrentCycleAction());
  };

  useEffect(() => {
    const stateJSON = JSON.stringify(cyclesState)

    localStorage.setItem('@timer:cycles-1.0.0', stateJSON)
  }, [cyclesState])

  return (
    <CycleContext.Provider
      value={{
        cycles,
        activeCycle,
        activeCycleId,
        setCycleToFinished,
        secondsOff,
        setSecondsPassed,
        createNewCycle,
        InterruptCurrentCycle,
      }}
    >
      {children}
    </CycleContext.Provider>
  );
};
