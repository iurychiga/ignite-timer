import { ThemeProvider } from 'styled-components';
import { GlobalStyle } from './styles/global.ts';
import { defaultTheme } from './styles/themes/default';

import { Router } from './Router';
import { CyclesContextProvider } from './contexts/CyclesContext.tsx';

function App() {
  return (
    <ThemeProvider theme={defaultTheme}>
      <CyclesContextProvider>
        <Router />
        <GlobalStyle />
      </CyclesContextProvider>
    </ThemeProvider>
  );
}
export default App;
