import { styled } from 'styled-components';

export const HomeContainer = styled.main`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  form {
    display: flex;
    flex-direction: column;
    gap: 3.5rem;
    align-items: center;
  }
`;

export const BaseCountDown = styled.button`
  width: 100%;
  border: 0;
  border-radius: 8px;
  padding: 1rem;

  display: flex;
  align-items: center;
  justify-content: center;

  gap: 0.5rem;
  font-weight: bold;

  transition: ease-in-out 0.2s;

  color: ${(props) => props.theme['gray-100']};

  &:disabled {
    opacity: 0.7;
    cursor: not-allowed;
    color: ${(props) => props.theme['gray-500']};
  }
`;

export const StartCountDown = styled(BaseCountDown)`
  background-color: ${(props) => props.theme['green-500']};

  &:not(:disabled):hover {
    background-color: ${(props) => props.theme['green-700']};
    color: ${(props) => props.theme['gray-500']};
  }
`;
export const StopCountDown = styled(BaseCountDown)`
  background-color: ${(props) => props.theme['red-500']};

  &:not(:disabled):hover {
    background-color: ${(props) => props.theme['red-700']};
    color: ${(props) => props.theme['gray-500']};
  }
`;
