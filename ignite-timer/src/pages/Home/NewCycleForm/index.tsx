import { FormContainer, MinutesAmountInput, TaskInput } from './styles';
import { useContext } from 'react';
import { useFormContext } from 'react-hook-form';
import { CycleContext } from '../../../contexts/CyclesContext';

export const NewCycleForm = () => {
  const { activeCycle } = useContext(CycleContext);
  const { register } = useFormContext();

  return (
    <FormContainer>
      <label htmlFor='task'>Vou trabalhar em</label>
      <TaskInput
        id='task'
        list='task-option'
        placeholder='Dê um nome para o seu projeto'
        disabled={!!activeCycle}
        {...register('task')}
      />

      <datalist id='task-option'>
        <option value='option 1' />
        <option value='option 2' />
        <option value='option 3' />
      </datalist>

      <label htmlFor='minutesAmount'>Durante</label>
      <MinutesAmountInput
        id='minutes'
        type='number'
        placeholder='00'
        disabled={!!activeCycle}
        step={1}
        min={1}
        max={60}
        {...register('minutes', { valueAsNumber: true })}
      />

      <span>minutos.</span>
    </FormContainer>
  );
};
