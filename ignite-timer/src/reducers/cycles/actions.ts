import { ActionTypes, iCycle } from '../../interfaces/validator';

export const addNewCycleAction = (newCycle: iCycle) => {
  return {
    type: ActionTypes.ADD_NEW_CYCLE,
    payload: {
      newCycle,
    },
  };
};

export const setCycleToFinishedAction = () => {
  return {
    type: ActionTypes.FINISHED_CURRENT_CYCLE,
  };
};

export const InterruptCurrentCycleAction = () => {
  return {
    type: ActionTypes.INTERRUPT_CURRENT_CYCLE,
  };
};
