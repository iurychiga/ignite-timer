import * as zod from 'zod';

// Schemas
export const newCycleFormValidateSchema = zod.object({
  task: zod.string().min(1, 'Informe a tarefa'),
  minutes: zod.number().min(1).max(60),
});

// types
export type NewCyclesFormData = Zod.infer<typeof newCycleFormValidateSchema>;

// interfaces

export interface iContextProps {
  children: React.ReactNode;
}
export interface CreateCycleData {
  task: string;
  minutes: number;
}

export interface iCycle {
  id: string;
  task: string;
  minutes: number;
  startDate: Date;
  interruptedDate?: Date;
  finishedDate?: Date;
}

export interface iCycleContext {
  activeCycle: iCycle | undefined;
  activeCycleId: string | null;
  secondsOff: number | undefined;
  cycles: iCycle[];
  setCycleToFinished: () => void;
  setSecondsPassed: (seconds: number) => void;
  createNewCycle: (data: CreateCycleData) => void;
  InterruptCurrentCycle: () => void;
}

export interface iCycleState {
  cycles: iCycle[];
  activeCycleId: string | null;
}

export enum ActionTypes {
  ADD_NEW_CYCLE = 'ADD_NEW_CYCLE',
  INTERRUPT_CURRENT_CYCLE = 'INTERRUPT_CURRENT_CYCLE',
  FINISHED_CURRENT_CYCLE = 'FINISHED_CURRENT_CYCLE',

}