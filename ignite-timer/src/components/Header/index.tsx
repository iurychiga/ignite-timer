import { HeaderContainer } from './styles';
import Logo from '../../assets/Logo.svg';
import { Timer, Scroll } from 'phosphor-react';
import { NavLink } from 'react-router-dom';

export const Header = () => {
  return (
    <HeaderContainer>
      <img src={Logo} alt=' ' />
      <nav>
        <NavLink to={'/'}>
          <Timer size={26} />
        </NavLink>
        <NavLink to={'/history'}>
          <Scroll size={26} />
        </NavLink>
      </nav>
    </HeaderContainer>
  );
};
